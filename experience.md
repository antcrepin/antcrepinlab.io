---
title: Experience
layout: default
use_fontawesome: true
permalink: /:basename
---

<h1 class="section-title">Career</h1>

<div class="row content-row">
<div class="col-12 col-sm-4">
    <p align="center">
        <img src="{{ site.baseurl }}/images/companies/decisionbrain.png" width="80%">
    </p>
</div>
<div class="col-12 col-sm-8 justified">
    <h3>Operations Research Engineer</h3>
    <p><i class="fa fa-building"></i> <i>DecisionBrain</i><br>
	<i class="fa fa-map-marker-alt"></i> <i>Paris, France</i><br>
    <i class="fa fa-calendar-alt"></i> <i>Since April 2021</i></p>
</div>
</div>

<h1 class="section-title">Internships</h1>

<div class="row content-row">
<div class="col-12 col-sm-4">
    <p align="center">
        <img src="{{ site.baseurl }}/images/companies/airfrance_klm_group.jpg" width="80%">
    </p>
</div>
<div class="col-12 col-sm-8 justified">
    <h3>Operations Research Engineer</h3>
	<p><i>End-of-studies internship</i></p>
    <p><i class="fa fa-building"></i> <i>Air France-KLM</i><br>
	<i class="fa fa-map-marker-alt"></i> <i>Orly, France</i><br>
    <i class="fa fa-calendar-alt"></i> <i>July 2020 - December 2020</i></p>
    <p>Stochastic optimization applied to the assignment of aircrafts to parking positions.</p>
    <ul>
	  <li>Built a <b>multiobjective stochastic model</b> including the needs of the company.</li>
	  <li>Developed an <b>optimization tool</b> (in Julia) incorporating scenarios describing the random disruptions of the flight schedule.</li>
	  <li>Studied the opportunity to switch from a deterministic planning approach to a stochastic one.</li>
	</ul>
    <p>The scientific report (written in French) is accessible <a href="{{ site.baseurl }}/documents/Memoire_PFE_Crepin_AF.pdf">here</a>. This work has been recognized with the following honors:<ul>
    <li>finalist of the <a href="https://www.roadef.org/societe-francaise-recherche-operationnelle-aide-decision">ROADEF</a> Master Student Prize 2020 (French Operations Research association);</li>
    <li>2nd prize of the <a href="https://www.ensta.org/fr">ENSTA Alumni</a> End-of-studies Internship Prize 2020.</li>
    </ul></p>
</div>
</div>
<hr>

<div class="row content-row">
<div class="col-12 col-sm-4">
    <p align="center">
        <img src="{{ site.baseurl }}/images/companies/edf_innovation_lab.png" width="70%">
    </p>
</div>
<div class="col-12 col-sm-8 justified">
    <h3>Energy Analyst</h3>
	<p><i>Gap year internship</i></p>
    <p><i class="fa fa-building"></i> <i>Électricité de France Innovation Lab</i><br>
	<i class="fa fa-map-marker-alt"></i> <i>Los Altos, CA, USA</i><br>
    <i class="fa fa-calendar-alt"></i> <i>March 2019 - August 2019</i></p>
    <p>Techno-economic modeling and optimization of <a href="https://www.edf-innovation-lab.com/thermal-microgrid-project/">multi-energy microgrids</a> (heating, cooling and power services).</p>
	<ul>
	  <li><b>Technical study</b> of <a href="https://sustainable.stanford.edu/campus-action/stanford-energy-system-innovations-sesi">Stanford Energy Systems Innovations</a> (SESI): Stanford University managed to switch from a 100% fossil fuel-based energy system to a grid-powered microgrid system based on heat recovery and energy storage.</li>
	  <li><b>Conception and development of a Python-driven pre-feasibility tool for multi-energy microgrids</b> (from scratch). The tool provides the optimal design for a new system (economic optimization) as well as energy and economic metrics (environmental benefits, financial viability).</li>
	  <li><b>Validation</b> of the tool based on use cases.</li>
	  <li><b>Application</b>: “Would a SESI-like system make sense on a smaller scale campus?”<br>Ran a pre-feasibility study for Foothill College in Los Altos Hills, CA.</li>
	</ul> 
</div>
</div>
<hr>

<div class="row content-row">
<div class="col-12 col-sm-4">
    <p align="center">
        <img src="{{ site.baseurl }}/images/companies/artelys.jpg" width="70%">
    </p>
</div>
<div class="col-12 col-sm-8 justified">
    <h3>Optimization Consultant</h3>
	<p><i>Gap year internship</i></p>
    <p><i class="fa fa-building"></i> <i>Artelys</i><br>
	<i class="fa fa-map-marker-alt"></i> <i>Paris, France</i><br>
    <i class="fa fa-calendar-alt"></i> <i>September 2018 - February 2019</i></p>
	<ol>
	  <li><b>Development of a route optimizer (Python-driven plugin for a GIS software)</b> helping for the design of heating and cooling networks on a city scale. Contribution to an <a href="http://planheat.eu/the-planheat-tool">open-source project</a>.</li>
	  <li><b>Analysis of energy data</b> (consumption, production) for the metropolis of Toulouse, France. Contribution to the elaboration of a master plan for the city of Toulouse to meet the energy objectives in terms of reduction of greenhouse gas emissions and use of renewable energies.</li>
	</ol> 
</div>
</div>