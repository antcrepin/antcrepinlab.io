---
title: Several ways to compute Fibonacci numbers
layout: post
use_fontawesome: true
use_code: true
use_math: true
use_toc: true
permalink: /posts/:title
---


<br>

## Introduction
---

<p class="centered">
    <img src="{{ site.baseurl }}/images/fibonacci-numbers/golden_ratio_nature.jpg" style="width: 50%">
</p>

<div class="justified">
    <p><span class="font-weight-bold">Abstract:</span> The Fibonacci sequence is one of the most famous formulas in mathematics. Each number in the sequence is the sum of the two numbers that precede it. We compare four methods to compute a specific iterate of the sequence.</p>
    <p><span class="font-weight-bold">Keywords:</span> Fibonacci numbers, recursion, memory, complexity</p>
	<p><span class="font-weight-bold">Programming language:</span> Python</p>
    <a href="https://gitlab.com/antcrepin/fibonacci-numbers" class="btn btn-light">
        <i class="fab fa-gitlab"></i> Repo
    </a>
</div>

<br>

## Definition
---

The Fibonacci sequence, commonly denoted $$(F_n)_{n\in\mathbb{N}}$$, is a sequence of numbers such that each number is the sum of the two preceding ones, starting from 0 and 1.

$$
\begin{cases}
F_0 = 0,\\
F_1 = 1,\\
F_n = F_{n-1} + F_{n-2} & \forall n\geq 2.
\end{cases}
$$

Thus the beginning of the sequence is: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144...

<br>

## Computation methods
---

The methods we compare are designed to compute the iterate $$F_n$$, with $$n\geq 0$$ given by the user.

<br>

### Method 1: primitive recursion

The primitive recursive solution is obtained by applying the definition formula "as is".

```python
def compute_fibonacci_naively(n):
    if n < 2:
        return n
    return compute_fibonacci_naively(n-1) + compute_fibonacci_naively(n-2)
```

This computation method turns out to be *very* inefficient. The reason for this is that for each number calculated, it needs to calculate all the previous numbers more than once.

<p class="centered">
    <img src="{{ site.baseurl }}/images/fibonacci-numbers/fibonacci_tree.png" style="width: 40%">
</p>

The image represents the calculation of $$F_5$$ using the function $$\texttt{compute_fibonacci_naively}$$. It computes the value of $$F_2$$ three times, and the value of $$F_1$$ five times. That just gets worse and worse when $$n$$ increases.

**A proof of the inefficiency of this method**

We can show that the number of calls to the function $$\texttt{compute_fibonacci_naively}$$ to compute $$F_n$$ is exponential with respect to $$n$$. Let $$A_n$$ be this number. We are going to show that

$$
A_n=2F_{n+1}-1.
$$

- $$n=0$$. $$\texttt{compute_fibonacci_naively}$$ is called once: $$A_0 = 1 = 2F_1-1$$.
- $$n=1$$. $$\texttt{compute_fibonacci_naively}$$ is called once: $$A_1 = 1 = 2F_2-1$$.
- *Induction.* Assume the result is true from 0 to $$n$$, let's demonstrate its validity for $$n+1$$.

$$
\begin{aligned}
A_{n+1} &= A_n + A_{n-1} + 1 \\
&= 2(F_{n+1} + F_n) - 1 \\
&= 2F_{n+2} - 1
\end{aligned}
$$

To complete the proof, we can use **Binet's formula**:

$$
F_n = \frac{\varphi^n-\psi^n}{\sqrt{5}},
$$

where $$\varphi=\frac{1+\sqrt{5}}{2}\approx 1.6180339887\dots$$ is the **golden ratio**, and $$\psi=-\frac{1}{\varphi} \approx -0.6180339887\dots$$

$$\varphi$$ and $$\psi$$ are the solutions of the equation $$x^2=x+1$$.

$$\lim_{n\rightarrow \infty}{\psi^n} = 0$$ so $$F_n\sim\frac{\varphi^n}{\sqrt{5}}$$ as $$n$$ tends to infinity. Then

$$
A_n\sim\frac{2\varphi^{n+1}}{\sqrt{5}}.
$$

The complexity of the primitive method is $$O(\varphi^n)$$.

<br>

### Method 2: use of memory

After studying the first method, a natural idea is to avoid doing the same calculations more than once by keeping the previous iterates in memory.

```python
def compute_fibonacci_sequence(n):
    # sequence initialized with [0] if n = 0; [0, 1] otherwise
    sequence = list(range(min(n, 1) + 1))
    if n >= 2:
        for i in range(2, n+1):
            # compute sequence[i] = F_i
            sequence.append(sequence[i-1] + sequence[i-2])
    return sequence

def compute_fibonacci_with_memory(n):
    sequence = compute_fibonacci_sequence(n)
    # return last element = F_n
    return sequence[-1]
```

The complexity becomes $$O(n)$$ here, which is a significant improvement. 

However the use of this method when we only need to compute a specific iterate is questionable. Indeed we are forced to keep *all* the iterates in memory.

<br>

### Method 3: iterative way

A 2-dimensional system of linear difference equations that describes the Fibonacci sequence is

$$
\begin{pmatrix}
F_{n+1}\\ 
F_{n}
\end{pmatrix}
=
\begin{pmatrix}
1 & 1\\ 
1 & 0
\end{pmatrix}
\begin{pmatrix}
F_{n}\\ 
F_{n-1}
\end{pmatrix}
\quad \forall n\geq 1.
$$

From this we can deduce a third method, which just consists in counting up without saving the intermediary values in a list.

```python
def compute_fibonacci_iteratively(n):
    current_iterate, next_iterate = 0, 1
    for i in range(n):
        # i from 0 to n-1
        # after iter i: (current_iterate, next_iterate) = (F_{i+1}, F_{i+2})
        current_iterate, next_iterate = next_iterate, current_iterate + next_iterate
    return current_iterate
```

The complexity is also $$O(n)$$ here but this method is expected to be faster than the previous one since there is no memory list to maintain.

<br>

### Method 4: advanced recursion

It is possible to compute Fibonacci numbers with complexity $$O(\log n)$$ by taking advantage from the following identities:

$$
\begin{cases}
F_{2n-1} = F_n^2 + F_{n-1}^2 \\
F_{2n} = (F_{n+1} + F_{n-1})F_n = (2F_{n+1} - F_n)F_n
\end{cases}
\quad \forall n\geq 1.
$$

```python
def compute_fibonacci_advanced_recursion(n):
    # return (F_n, F_{n+1})
    if n == 0:
        return 0, 1
    # a = F_{n//2}, b = F_{n//2 + 1}
    a, b = compute_fibonacci_advanced_recursion(n//2)
    # u = F_{2 * (n//2)}, v = F_{2 * (n//2) + 1}
    u = (2*b - a)*a
    v = a**2 + b**2
    if n % 2 == 0:
        return u, v
    return v, u + v

def compute_fibonacci_advanced(n):
    return compute_fibonacci_advanced_recursion(n)[0]
```

<br>

## Comparison
---

For $$n=200000$$, we compare the four methods:

```
(env) [...]/fibonacci> python comparison.py

Comparison:
> compute_fibonacci_naively
Error: maximum recursion depth exceeded in comparison
Time: 1.551E-02 s

> compute_fibonacci_with_memory
Time: 8.603E+00 s

> compute_fibonacci_iteratively
Time: 1.561E+00 s

> compute_fibonacci_advanced
Time: 3.826E-02 s
```

As expected, $$\texttt{compute_fibonacci_advanced}$$ is the most efficient method. In second place comes $$\texttt{compute_fibonacci_iteratively}$$.

The primitive algorithm $$\texttt{compute_fibonacci_naively}$$ performs very poorly, even for small values of $$n$$ (around 30 seconds needed to compute $$F_{40}$$).

<br>

If we need to compute *all* the numbers of the sequence from 0 to $$n$$, it's not recommended to use

```python
sequence = [compute_fibonacci_advanced_recursion(i) for i in range(n+1)]
```

or

```python
sequence = [compute_fibonacci_iteratively(i) for i in range(n+1)]
```

$$\texttt{compute_fibonacci_sequence}$$ would be the best method to use.

<br>

## Related
---

### Golden ratio approximation

Johannes Kepler observed that the ratio of consecutive Fibonacci numbers converges. He wrote that:

> "As 5 is to 8 so is 8 to 13, practically, and as 8 is to 13, so is 13 to 21 almost"

and he concluded that these ratios approach the golden ratio $$\varphi$$. As a matter of fact,

$$
\lim_{n\rightarrow \infty}{\frac{F_{n+1}}{F_n}} = \varphi.
$$

Even for small values of $$n$$, the ratio of two consecutive Fibonacci numbers is a really good approximation of $$\varphi$$.

<p class="centered">
    <img src="https://gitlab.com/antcrepin/fibonacci-numbers/-/raw/develop/plots/golden_ratio_approximation.png" style="width: 66%">
</p>

<br>

### Golden spiral

In geometry, a **golden spiral** is a logarithmic spiral whose growth factor is $$\varphi$$, the golden ratio. In other words, a golden spiral gets wider (or further from its origin) by a factor of $$\varphi$$ for every quarter turn it makes.

A golden spiral can be approximated using a **Fibonacci spiral**. A Fibonacci spiral starts with a rectangle partitioned into two squares. In each step, a square the length of the rectangle's longest side is added to the rectangle. We have seen that the ratio between consecutive Fibonacci numbers approaches the golden ratio as the Fibonacci numbers approach infinity. Similarly, the way this spiral approximates the golden spiral gets better as additional squares are added. 

<p class="centered">
    <img src="https://gitlab.com/antcrepin/fibonacci-numbers/-/raw/develop/plots/golden_spiral_approximation.png" style="width: 90%">
</p>

<br>

### Math and prog rock

:rotating_light: **Nerd alert!**

The American progressive metal band Tool structures the rhythm of certain parts of the song *Lateralus* according to the Fibonacci sequence.

<p class="centered">
    <iframe width="445" height="250" src="https://www.youtube.com/embed/Y7JG63IuaWs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

<br>

In his podcast, Joe Rogan described the writing process of Tool's lead singer Maynard James Keenan.

> "He wrote a song to the Fibonacci sequence. The Fibonacci sequence is a mathematical sequence. The way it works is it goes like 0, the next number is 1, and the next number is 2, and then 2+1 which is 3, and it keeps going in this mathematical progression. That’s how they had the chord progression. They had the chord progression link up to the Fibonacci sequence. **He’s a fucking maniac**."

The syllables Maynard sings in the first verse follow the first six numbers in the pattern, ascending and descending in the sequence **1-1-2-3-5-8-5-3**.

> "Black,<br>
> then,<br>
> white are,<br>
> all I see,<br>
> in my infancy.<br>
> Red and yellow then came to be,<br>
> reaching out to me.<br>
> Lets me see."

In the next verse, Maynard begins with the seventh number of the Fibonacci sequence (13), implying a missing verse in between. He descends back down with the following pattern: **13-8-5-3**.

> "As below so above and beyond I imagine.<br>
> Drawn beyond the lines of reason.<br>
> Push the envelope.<br>
> Watch it bend."

The second verse adds the missing line to complete the sequence: **2-1-1-2-3-5-8**.

> "There is,<br>
> so,<br>
> much,<br>
> more that,<br>
> beckons me,<br>
> to look through to these,<br>
> infinite possibilities."

<br>

### Nature and irrationality of $$\varphi$$

Numberphile: *The Golden Ratio (why is it so irrational)*

<p class="centered">
    <iframe width="445" height="250" src="https://www.youtube.com/embed/sj8Sg8qnjOg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>