---
title: Posts
layout: default
use_fontawesome: true
permalink: /:basename/
---

<h1 class="section-title">Optimization</h1>

<div class="row content-row">
<div class="col-12 col-sm-4 image-wrapper">
    <img src="{{ site.baseurl }}/images/flight-route-optimizer/breitling.jpg">
</div>
<div class="col-12 col-sm-8 justified">
    <h3>A route optimizer for the Breitling 100/24 race</h3>
    <p><span class="font-weight-bold">Abstract:</span> In the late 2000s, early 2010s, the watch manufacturing company Breitling organized airplane racing competitions in France every year. In order to win, a participant had to complete 100 “touch-and-go” maneuvers in distinct airdromes in a 24-hour timeframe by traveling a shorter distance than its competitors. Groups of airdromes are predefined and each group must be visited at least once to ensure some geographical diversity. The length of a stage must not exceed a certain maximum threshold distance based on the average fuel consumption of a plane and the capacity of its tank. We consider a simplified version of the problem where the time component is ignored (variant of the Traveling Salesman Problem). We consider four methods to solve the problem.</p>
    <p><span class="font-weight-bold">Keywords:</span> Traveling Salesman Problem (TSP), dynamic constraint generation, LP relaxation, MILP, total unimodularity</p>
	<p><span class="font-weight-bold">Programming language:</span> Julia</p>
    <a href="{{ site.baseurl }}{% link _posts/2020-06-19-flight-route-optimizer.md %}" class="btn btn-light">
        <i class="fa fa-link"></i> Post
    </a>
    <a href="https://gitlab.com/antcrepin/flight-route-optimizer" class="btn btn-light">
        <i class="fab fa-gitlab"></i> Repo
    </a>
</div>
</div>

<h1 class="section-title">Others</h1>

<div class="row content-row">
<div class="col-12 col-sm-4 image-wrapper">
    <img src="{{ site.baseurl }}/images/fibonacci-numbers/golden_ratio_nature.jpg">
</div>
<div class="col-12 col-sm-8 justified">
    <h3>Several ways to compute Fibonacci numbers</h3>
    <p><span class="font-weight-bold">Abstract:</span> The Fibonacci sequence is one of the most famous formulas in mathematics. Each number in the sequence is the sum of the two numbers that precede it. We compare four methods to compute a specific iterate of the sequence.</p>
    <p><span class="font-weight-bold">Keywords:</span> Fibonacci numbers, recursion, memory, complexity</p>
	<p><span class="font-weight-bold">Programming language:</span> Python</p>
    <a href="{{ site.baseurl }}{% link _posts/2021-05-15-fibonacci-numbers.md %}" class="btn btn-light">
        <i class="fa fa-link"></i> Post
    </a>
    <a href="https://gitlab.com/antcrepin/fibonacci-numbers" class="btn btn-light">
        <i class="fab fa-gitlab"></i> Repo
    </a>
</div>
</div>